cmake_minimum_required(VERSION 2.6.0)
project(project_name)
set(PROJECT_LINK_LIBS lib.so)

link_directories(/path/to/lib)
include_directories(/path/to/include/dir)
add_executable(exe exe.cpp)

target_link_libraries(exe ${PROJECT_LINK_LIBS})
