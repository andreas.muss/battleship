#include "login.h"
#include "util.h"
#include "rfc.h"
#include "score.h"
#include "clientthread.h"
#include "map.h"
#include "user.h"

#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>

as allsockets;
mc myconfig;
int server_socket;
int playerCounter;
int activePlayers = 0;
int mapsize = 10;

extern pi playerinfo[2];
extern ms mutex_struct;
extern pl playerlist[2];
extern Map map;

pthread_t login_thread;
pthread_t score_thread;


void *login_handler(void* noParam){
	infoPrint("Login-Thread wurde erfolgreich gestartet.");

	int server_socket;
  	int client_socket;
	ssize_t byteCount;
   	char client_message[2000];

  	struct sockaddr_in server;
  	struct client;
    
    pthread_mutex_init(&mutex_struct.playerlist_mutex, NULL);
    
    pthread_mutex_lock(&mutex_struct.playerlist_mutex);
    map = *init_map_matrix(2*mapsize, mapsize);
    pthread_mutex_unlock(&mutex_struct.playerlist_mutex);
    
  	server_socket=socket(AF_INET, SOCK_STREAM, 0);
  	int on = 1;
  	setsockopt(server_socket, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on));

  	if(server_socket<0){
    		errorPrint("Fehler beim Erstellen des Sockets");
		return 0;
  	}

  	debugPrint("Server Connection Informationen werden gesetzt");

  	server.sin_family=AF_INET;		//IPV4
  	server.sin_addr.s_addr=INADDR_ANY;	//Alle Adressen akzeptieren
  	server.sin_port=htons(myconfig.port);

  	debugPrint("Server Connection Informationen wurden gesetzt");

	//Socket an die obige Adresse binden
  	if(bind(server_socket,(struct sockaddr *)&server, sizeof(server))<0){
    		errorPrint("Fehler beim binden des Login-Sockets");
    		return 0;
  	}
  	debugPrint("Socket %i wurde erfolgreich gebunden", server_socket);
  	allsockets.server_socket=server_socket;


	//Socket wartet auf Connections a
  	if(listen(server_socket,4)){
   	 	errorPrint("Fehler beim Listening");
		return 0;
  	}



 	while(1){
   		client_socket = accept(server_socket, NULL, NULL);

   		if(GetGameCancelled() == 1)
   			return 0;

		if(client_socket < 0){
			errorPrint("Fehler beim akzeptieren des Sockets");
			return 0;
		}

		if(GetActivePlayers() == 4){
			sendERR(1,"Maximale Spieleranzahl erreicht.", client_socket);
			close(client_socket);
			continue;
		}

		if(GetGameIsRunning() == 1){
			sendERR(1,"Spiel laeuft bereits.", client_socket);
			close(client_socket);
			continue;
		}

		//Daten die ueber den Socket gesendet werden, werden gelesen

    	byteCount = recv(client_socket, client_message, sizeof(client_message),0);
   		if(byteCount<0){
			errorPrint("Fehler beim lesen!");
			return 0;
    		}

   		debugPrint("Login Thread wartet auf Message");

		//readMessage: liegt in der login.h, User-Daten werden auf validaet geprueft
		if(readMessage(client_message, client_socket, playerinfo)){	//server_data in playerinfo ändern
			//Client-Thread wird erzeugt.

			if(pthread_create(&(playerinfo[playerCounter].user_thread), NULL, client_thread, (void*) &client_socket)<0){	//server_data in playerinfo ändern
				errorPrint("Fehler beim Erstellen des Client-Threads.");
				return 0;
			}

			//Kritischer Abschnitt -- START --
			pthread_mutex_lock (&mutex_struct.playerlist_mutex);
			//sem_post(&score_semaphore);
			pthread_mutex_unlock (&mutex_struct.playerlist_mutex);
			// Kritischer Abschnitt -- ENDE --

		}
		playerCounter++;

		debugPrint("Neuer Spieler wurde gefunden und benutzt Socket %i", client_socket);

	}
  	debugPrint("Login Socket wird geschlossen");

 	close(server_socket);
return 0;
}

// Lesen und auswerten der LoginRequestNachricht vom Client
int readMessage(char client_message[], int client_socket, pi playerinfo[]){	//server_data in playerinfo ändern
	//int activePlayers=getPlayerAmount(playerinfo);	//server_data in playerinfo ändern
	
	//Message-Type wird geprüft
	if(client_message[0]==1){
		if(myconfig.receive){
			debugHexdump(client_message, sizeof(client_message)/sizeof(client_message[0]), "C==>S");
		}
	//Pruefung der Spieleranzahl
		if(activePlayers==2){
			sendERR(1,"Maximale Spieleranzahl erreicht.", client_socket);
			return -1;
		}
		
		pthread_mutex_lock(&mutex_struct.playerlist_mutex);
		playerinfo[activePlayers].socket = client_socket;
        pthread_mutex_unlock(&mutex_struct.playerlist_mutex);
        
        activePlayers++;

		sendLOK(activePlayers, client_socket);

	}
	return 1;
}


//Funktion zur Ausgabe der Start Parameter auf der Konsole
void printStartParameter(){
	errorPrint("Aufruf: ./server -p PORT [-d] [-m] [-r] [-s] [-h]\n");
 	errorPrint("[] -> Optional");
	errorPrint("-p = Port (Default: 9495)");
    errorPrint("[-d] = Debug meldungen");
	errorPrint("[-m] = Keine Farben fuer die Ausgabe auf der Konsole verwenden");
	errorPrint("[-r] = Empfangene Pakete als Hexdump ausgeben");
	errorPrint("[-s] = Gesendete Pakete als Hexdump ausgeben");
    errorPrint("[-h] = Zeigt diesen Dialog an");
}

//Setzen der default Werte
void setDefaults(){
	myconfig.port=9495;
	myconfig.debug=0;
	myconfig.monochrome=0;
	myconfig.receive=0;
	myconfig.send=0;
	playerCounter = 0;

	infoPrint("Battleship");

	//score_sockets.score=-1;
	signal(SIGINT, killHandler);
	//Das erste Byte aller Usernamen als Ende markieren
	for(int i=0;i<2;i++){
		playerinfo[i].score=0;		//player_ranking in playerinfo ändern
		playerlist[i].score=0;		//playerlist in playerinfo ändern
	}
}

//Erstellen des Lock-Files
int createLockfile(){
	int fd = open("Lock-File" , O_CREAT | O_EXCL, 0600);
	if(fd<0){
		return -1;
	}else{
		return 1;
	}
}

//Loeschen des Lock-Files
int removeLockfile(){
	int ret = remove("Lock-File");
	if(ret<0){
		return -1;
	}else{
		return 1;
	}
}

//Handler wird beendet
void killHandler(int sig){
	signal(sig, SIG_IGN);
	printf("\n");
	debugPrint("Programm wurde durch STRG-C gekillt.");
	removeLockfile();
	cleanUpSystem("Kill Signal erhalten");
	CloseServer();
	close(server_socket);
	kill(0, SIGQUIT);

	exit(0);
}

//Funktion zum aufräumen des Systems nach Beendigung
int cleanUpSystem(char errorMessage[25]){

	if(removeLockfile()<0){
		errorPrint("Fehler beim Entfernen des Lock-File.");
		return -1;
	}else{
		infoPrint("Lock-File wurde erfolgreich entfernt.");
	}
	//closeAllSockets();

	exit(0);

	return 1;
}

