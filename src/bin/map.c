#include <stdio.h>
#include <stdlib.h>

#include "map.h"
#include "ship.h"

Map map;

Map *init_map_matrix(int width, int height) {
    Map *m = malloc(sizeof(Map));
    int i,j;
    
    m->width = width;
    m->height = height;
    
    m->map = malloc(m->width * sizeof(int *));
    
    for(i=0; i<m->width; i++)
    {
        m->map[i] = malloc(m->height * sizeof(int));
        for(j=0; j<m->height; j++)
            m->map[i][j] = 0;
    }
    
    m->ships[0] = 2;
    m->ships[1] = 2;
    m->ships[2] = 2;
    m->ships[3] = 2;
    
    return m;
}

void show_map(Map* m)
{
    int i;
    
    // Print letters line
    for (i=0; i<m->height; i++)
    {
        debugPrint("%i | %i, %i, %i, %i, %i, %i, %i, %i, %i, %i, %i, %i, %i, %i, %i, %i, %i, %i, %i, %i", i, m->map[0][i], m->map[1][i], m->map[2][i], m->map[3][i], m->map[4][i], m->map[5][i],
            m->map[6][i], m->map[7][i], m->map[8][i], m->map[9][i], m->map[10][i], m->map[11][i], m->map[12][i], m->map[13][i], m->map[14][i], m->map[15][i], m->map[16][i], m->map[17][i], 
            m->map[18][i], m->map[19][i]
        );
    }
}

bool check_first_map(Map* m)
{
    int i, j;

    for(i=0; i<m->width/2; i++)
        for(j=0; j<m->height; j++)
            if (m->map[i][j] == SHIP)
                return false;
            
    return true;
}

bool check_second_map(Map* m)
{
    int i, j;

    for(i=0; i<m->width/2; i++)
        for(j=0; j<m->height; j++)
            if (m->map[i+(m->width/2)][j] == SHIP)
                return false;
            
    return true;
}

