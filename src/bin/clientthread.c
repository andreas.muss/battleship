#include "clientthread.h"
#include "rfc.h"
#include "login.h"
#include "user.h"
#include "score.h"
#include "map.h"
#include "ship.h"
#include "util.h"

#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <pthread.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <bits/errno.h>
#include <errno.h>
#include <stdio.h>
#include <poll.h>

extern pi playerinfo[2];
extern ms mutex_struct;
extern pl playerlist[2];
extern as allsockets;
extern mc myconfig;
extern int server_socket;
extern Map map;

static int activePlayers = 0;
static int gameIsStarted = 0;
static int gameIsFinished = 0;
static int finishedPlayers = 0;
static int gameCancelled = 0;

static int mapsize = 10;

static int attackresult = -2;

pthread_mutex_t activePlayer_mutex;
pthread_mutex_t gameIsStarted_mutex;
pthread_mutex_t gameIsFinished_mutex;
pthread_mutex_t finishedPlayers_mutex;
pthread_mutex_t gameCancelled_mutex;

sem_t sem;

int GetGameIsRunning() { return gameIsStarted; }
int GetActivePlayers() { return activePlayers; }
int GetGameCancelled() { return gameCancelled; }


/**************************************************
 ****************** Client Thread *****************
 **** wird fuer jeden Spieler einmal gestartet ****
 **************************************************/
void *client_thread(void* socket_desc)
{
	debugPrint("Client-Thread wurde erfolgreich gestartet");
	int client_socket = *(int*)socket_desc;

	debugPrint("Client Thread benutzt Socket %d", client_socket);

    sem_init(&sem, 0, 1);
    
    sem_wait(&sem);
	activePlayers++;
    sem_post(&sem);
    
    debugPrint("Anzahl der angemeldeten Spieler ist %d.", activePlayers);
    
    while(1)
	{
		char client_message[1024];
		memset(client_message, '\0', 1024);

        int recv_value;
        
        // Nachricht empfangen
        recv_value = read(client_socket, client_message, sizeof(client_message));
        debugPrint("recv_value: %d", recv_value);
        
        // Das Spiel ist nicht mehr aktiv
        // Alle Client Threads müssen beendet werden
        if(gameCancelled == 1) {
            debugPrint("Client-Thread wird beendet, das Spiel wurde beendet");
            return 0;
        }


        /************************************************
        ************** Client hat Disconnect ***********
        ************************************************/
        if(recv_value == 0)
        {
            if(isGameLeader(client_socket))
            {
                /************************************************
                *** Fehlermeldung an alle Clients senden *******
                ****** Kritischer Abschnitt Start Sockets ******
                ************************************************/
                pthread_mutex_lock(&mutex_struct.socket_mutex);
                for(int i=0;i<2;i++){
                    if(playerinfo[i].socket !=0)
                        sendERR(1,"Spielleiter hat das Spiel verlassen.", playerinfo[i].socket);
                }
                pthread_mutex_unlock(&mutex_struct.socket_mutex);
                /****** Kritischer Abschnitt Ende Sockets *******/

                gameCancelled = 1;

                debugPrint("Spielleiter hat das Spiel verlassen - CloseServer wird aufgerufen");
                CloseServer();

                return 0;
            }

            /************************************************
            ************ Liste aktualisiern *****************
            ************************************************/
            pthread_mutex_lock (&mutex_struct.playerlist_mutex);
            debugPrint("Spieler hat das Spiel verlassen");
            debugPrint("Spielerliste wird sortiert");
            sortPlayerList(client_socket);
            pthread_mutex_unlock (&mutex_struct.playerlist_mutex);

            // Spieleranzahl herunterzählen
            activePlayers--;

            if(GetActivePlayers() < 2) {
                for(int i = 0; i < MAX_PLAYERS; i++) {
                    if(playerinfo[i].socket != 0)
                        sendERR(1, "Zu wenig Spieler.", playerinfo[i].socket);
                }


            }


            close(client_socket);
            pthread_exit(NULL);
            return 0;

        }

        // Error Handling
        if(recv_value < 0)
        {
            CloseServer();
            errorPrint("Fehler beim Empfangen der Nachrichten im Client-Thread");
            pthread_exit(NULL);
        }
        
        if(myconfig.send)
        {
            debugPrint("Folgende Nachricht wurde von Socket %i empfangen:", client_socket);
            debugHexdump(client_message, recv_value, "S<==C");
        }
        
        encodeMessage(client_message, client_socket, recv_value);
	}
}

void sigquit(){
	infoPrint("Main Prozess wird beendet");
	close(server_socket);
	exit(0);
}

void CloseServer()
{
	debugPrint("Aufräum Aktionen werden ausgeführt");
	removeLockfile();

	// Login Socket schliessen
	errorPrint("Login Socket wird geschlossen");
	close(allsockets.server_socket);
	shutdown(allsockets.server_socket, 2);

	errorPrint("Der Server wird jetzt beendet");
	kill(0, SIGQUIT);
}

void encodeMessage(char message[], int client_socket, int length){

	char filename[length - 3];
	memset(filename, '\0', length);
    int thisPlayer = getPlayerOfThread(client_socket);
    switch(message[0])
	{
		// Start Game Message
		case STG_TYPE:
			if(!isGameLeader(client_socket)) {
				sendERR(0, "Keine Berechtigung zum Starten des Spiels", client_socket);
				return;
			}

			pthread_mutex_lock(&mutex_struct.playerlist_mutex);
			if(playerinfo[1].socket == 0) {
                debugPrint("SocketID %d.", playerinfo[1].socket);
				sendERR(0, "Es müssen 2 Spieler zum Starten des Spiels anwesend sein", client_socket);
				return;
			}
			pthread_mutex_unlock(&mutex_struct.playerlist_mutex);

			infoPrint("Der Spielleiter hat das Spiel gestartet");

			for(int i=0; playerinfo[i].socket !=0 && i < 2;i++){
				if(myconfig.send){
					debugHexdump(message, message[2]+3, "S==>C");
				}
				send(playerinfo[i].socket, message, message[2]+3,0);
                debugPrint("STG gesendet an Socket %d", playerinfo[i].socket);
			}

			gameIsStarted = 1;

			break;
            
        // Ship Message
        case SHP_TYPE:
            // new ships are placed
            if(message[1] != 4) {
                sendERR(0, "Es sind nur genau 4 Schiffe erlaubt", client_socket);
            } else {
                int messagecount = 0;
                // Player 1 placed his ships on the left half of the map
                if(client_socket == playerinfo[0].socket)   {
                    // insert_ship(Map, ShipType, ShipXPos, ShipYPos, ShipOrient)
                    while(messagecount<16)  {
//                         pthread_mutex_lock(&mutex_struct.playerlist_mutex);
                        debugPrint("Schiffe werden in die linke Karte eingefügt!");
                        debugPrint("Schiff wird an Position (%i,%i) eingefügt!", message[messagecount+3], message[messagecount+4]);
                        if (insert_ship(&map, message[messagecount+2], message[messagecount+3], message[messagecount+4], message[messagecount+5]) != 0)
                            errorPrint("inser_ship failed!");
                        show_map(&map);
//                         pthread_mutex_lock(&mutex_struct.playerlist_mutex);
                        messagecount = messagecount + 4;
                    }
                // Player 2 placed his ships on the right halt of the map
                } else if(client_socket == playerinfo[1].socket)    {
                    // insert_ship(Map, ShipType, ShipXPos, ShipYPos, ShipOrient)
                    while(messagecount<16)  {
//                         pthread_mutex_lock(&mutex_struct.playerlist_mutex);
                        debugPrint("Schiffe werden in die rechte Karte eingefügt!");
                        debugPrint("Schiff wird an Position (%i,%i) eingefügt!", (message[messagecount+3]+mapsize), message[messagecount+4]);
                        if (insert_ship(&map, message[messagecount+2], (message[messagecount+3]+mapsize), message[messagecount+4], message[messagecount+5]) != 0)
                            errorPrint("insert_ship failed!");
                        show_map(&map);
//                         pthread_mutex_unlock(&mutex_struct.playerlist_mutex);
                        messagecount = messagecount + 4;
                    }
                } else {
                    sendERR(0, "Fehler beim parsen der Schiffe", client_socket);
                }
            }
            break;
            
        // Attack Request Message
        case ARQ_TYPE:
            // handle ATTACK request
            // Player 1 attacks the right half of the map
            pthread_mutex_lock(&mutex_struct.playerlist_mutex);
            if(client_socket == playerinfo[0].socket)   {
                attackresult = attack_ship(&map, message[1]+mapsize, message[2]);
            // PLayer 2 attacks the left half of the map
            } else if(client_socket == playerinfo[1].socket)    {
                attackresult = attack_ship(&map, message[1], message[2]);
            }
            pthread_mutex_unlock(&mutex_struct.playerlist_mutex);
            
            if(attackresult == -2)  {
                sendERR(0, "ARQ Nachricht wurde nicht korrekt verarbeitet", client_socket);
            } else if(attackresult == -1)   {
                sendERR(0, "ungültige x oder y Position in ARQ", client_socket);
            } else if(attackresult == 0)    {
                pthread_mutex_lock(&mutex_struct.playerlist_mutex);
                for(int i=0; playerinfo[i].socket !=0 && i < 2;i++){
                    sendARE(message[1], message[2], attackresult, playerinfo[i].socket);
                }
                pthread_mutex_unlock(&mutex_struct.playerlist_mutex);
            } else if(attackresult == 1)    {
                pthread_mutex_lock(&mutex_struct.playerlist_mutex);
                for(int i=0; playerinfo[i].socket !=0 && i < 2;i++){
                    sendARE(message[1], message[2], attackresult, playerinfo[i].socket);
                }
                pthread_mutex_unlock(&mutex_struct.playerlist_mutex);
            }
            
            show_map(&map);
            
            // check if the first half of the map contains ships
            if(check_first_map(&map))   {
                debugPrint("Spieler 2 gewinnt das Spiel!");
                sendGOV();
            // check if the second half of the map contains ships
            } else if(check_second_map(&map))   {
                debugPrint("Spieler 1 gewinnt das Spiel!");
                sendGOV();
            }
            
            break;
            
		// Unbekannte Nachricht
		default:
			sendERR(0, "Unbekannter Nachrichtentyp", client_socket);
			break;

	}

}


int getPlayerOfThread(int client_socket){
	int thisPlayer;
	for(int i=0;i<2;i++){
		if(playerinfo[i].socket == client_socket){
			thisPlayer=i;
		}
	}
	return thisPlayer;
}
