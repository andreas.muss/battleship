#ifndef LOGIN_H
#define LOGIN_H
#include "rfc.h"

int readMessage(char client_message[], int client_socket, pi playerinfo[]);	//server_data in playerinfo ändern
int getPlayerAmount(pi playerinfo[]);	//server_data in playerinfo ändern
int checkPlayerName(char client_message[], pi playerinfo[], int activePlayers);	//server_data in playerinfo ändern
int createLockfile();
int removeLockfile();
int cleanUpSystem(char errorMessage[25]);
void printStartParameter();

void killHandler(int sig);
void druckeFehler();
void setDefaults();

void *login_handler(void* noParam);

//nach Auslagerung von clientSocket kann server_socket als globale Variable deklariert werden
typedef struct __attribute__((packed)){
	//int clientSocket[4];		//wurde in playerinfo ausgelagert
	int server_socket;
}as;

typedef struct __attribute__((packed)){
    int port;
    int monochrome;
    int receive;
    int send;
    int debug;
}mc;

int GetLoginSocket();

#endif
