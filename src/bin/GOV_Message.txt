GOV Nachricht:
-------------------------------------------------
|   Type    |   length  |   rank    |   score   |
-------------------------------------------------

Type:   7
length: Länge der Nachricht (default: 4)
rank:   Platzierung des Spielers
score:  Punktzahl des Spielers (nicht in Benutzung)
