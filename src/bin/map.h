#ifndef MAP_H
#define MAP_H

#include <stdbool.h>
#include <util.h>

#define WATER 0
#define MISS 1
#define HIT 2
#define SHIP 3

typedef struct map {
    int width;
    int height;
    int **map;
    
    int ships[4];
    int total;
}Map;

Map *init_map_matrix(int,int);
void show_map(Map *m);
bool check_first_map(Map* m);
bool check_second_map(Map* m);

#endif
