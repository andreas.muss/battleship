#include <stdio.h>

#include "map.h"
#include "ship.h"

// x=width y=height
int insert_ship(Map* m, int ship, int x, int y, int orientation)
{
    int i, size;
    
    switch(ship)
    {
        case AIRCRAFT_CARRIER:
            size = AIRCRAFT_CARRIER_SIZE;
            break;
        case BATTLESHIP:
            size = BATTLESHIP_SIZE;
            break;
        case SUBMARINE:
            size = SUBMARINE_SIZE;
            break;
        case PATROL_BOAT:
            size = PATROL_BOAT_SIZE;
            break;
    }
    
    debugPrint("Width: %i, Height: %i", m->width, m->height);
    
    if (orientation == VERTICAL){
        if (x >= m->width || y+size-1 >= m->height){
            return -1;
        } else {
            for (i=y; i<y+size; i++)
                if (m->map[x][i] != 0)
                    return -1;
                
            for (i=y; i<y+size; i++)
                m->map[x][i] = SHIP;
        }
    }
        
    if (orientation == HORIZONTAL){
        if (y >= m->height || x+size-1 >= m->width){
            return -1;
        } else {
            for (i=x; i<x+size; i++)
                if (m->map[i][y] != 0)
                    return -1;
                
            for (i=x; i<x+size; i++)
                m->map[i][y] = SHIP;
        }
    }
    
    m->ships[ship]--;
    
    return 0;
}

int attack_ship(Map *m, int x, int y)
{
    if (x > m->width || x < 0 || y > m->width || y < 0)
        return -1;

    if (m->map[x][y] == SHIP || m->map[x][y] == HIT)
    {
        m->map[x][y] = HIT;
        return 1;
    } else {
        m->map[x][y] = MISS;
        return 0;
    }
}
