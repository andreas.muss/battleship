\documentclass[12pt,dvipsnames]{scrartcl}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[ngerman]{babel}
\usepackage{charter}

\usepackage{fancyhdr}

\pagestyle{fancy}
\fancyhf{}
\fancyhead[L]{Informatikprojekt}
\fancyhead[R]{Andreas Muss, Florian Ruoff}
\fancyfoot[C]{\leftmark}
\fancyfoot[L]{\thepage}

\usepackage{ragged2e}

\usepackage[backend=bibtex]{biblatex}

\bibliography{literatur}

\usepackage{graphicx}
\usepackage{tikz}
\usetikzlibrary{arrows,decorations.pathmorphing,shapes}

\usepackage{logicpuzzle}

\usepackage{dirtree}

\usepackage{algorithm2e}
\usepackage{float}

\usepackage{caption}
\captionsetup{labelfont={bf,small},textfont={small}}

\usepackage{xcolor}
\usepackage{listings}
\renewcommand{\lstlistingname}{Code}

\definecolor{mGreen}{rgb}{0,0.6,0}
\definecolor{mGray}{rgb}{0.5,0.5,0.5}
\definecolor{mPurple}{rgb}{0.58,0,0.82}
\definecolor{backgroundColour}{rgb}{1,1,1}

\lstdefinestyle{CStyle}{
%	basicstyle=\linespread{0.83}\small\ttfamily,
	backgroundcolor=\color{backgroundColour},   
	basicstyle=\footnotesize\ttfamily,
	keywordstyle=\bfseries\color{green!40!black},
	commentstyle=\itshape\color{purple!40!black},
	identifierstyle=\color{blue},
	stringstyle=\color{orange}
	breakatwhitespace=false,         
	breaklines=true,                 
	captionpos=b,                    
	keepspaces=true,                 
	numbers=left,                    
	numbersep=5pt,                  
	showspaces=false,                
	showstringspaces=false,
	showtabs=false,                  
	tabsize=2,
	language=C
}

\usepackage{fancyvrb}

% redefine \VerbatimInput
\RecustomVerbatimCommand{\VerbatimInput}{VerbatimInput}%
{fontsize=\footnotesize,
	%
	frame=lines,  % top and bottom rule only
	framesep=2em, % separation between frame and text
	rulecolor=\color{Gray},
	%
	labelposition=topline,
}

\usepackage{microtype}

\usepackage{hyperref}

\usepackage{acronym}

\newacro{Sysprog}[SysProg]{Systemprogrammierung}
\newacro{GraBo}[GraBo]{Grafische Bedienoberflächen}

\begin{document}
	\begin{titlepage}
		\centering
		\includegraphics[width=0.5\linewidth]{welle.png}\\
		\vspace{1cm}
		{\LARGE Schiffe versenken - eine Client-Server Anwendung}\\
		\vspace{1cm}
		{\Large Muss, Andreas\\
		\Large 27397\\
		\texttt{andreas.muss@hs-weingarten.de}\\
		\vspace{0.5cm}
		Ruoff, Florian\\
		\Large 27436\\
		\texttt{florian.ruoff@hs-weingarten.de}\\
		\vfill
		\textbf{Informatikprojekt}\\
		unter Leitung von\\
		Prof. Dr. Silvia \textsc{Keller}}\\
		\vfill
		\today
	\end{titlepage}
	
	\pagenumbering{roman}
	\setcounter{page}{2}
	\thispagestyle{plain}
	\vspace*{\fill}
	{\Large Eidesstattliche Erklärung\par}
	\begin{justify}
		\rule[0.5cm]{\linewidth}{1pt}
		Wir versichern an Eides statt, dass wir die vorliegende Arbeit selbstständig verfasst und keine andere als die angegebenen Quellen und Hilfsmittel benutzt habe. Die aus fremden Quellen direkt oder indirekt übernommenen Gedanken sind als solche gekennzeichnet. Die Arbeit wurde bisher in gleicher oder ähnlicher Form keiner anderen Hochschule vorgelegt oder veröffentlicht.
	\end{justify}
	\vspace{3cm}
	\rule{5cm}{0.5pt}	\hfill	\rule{5cm}{0.5pt}
	
	\hspace{30pt}Ort/Datum	\hfill	Unterschrift
	\hspace{35pt}
	\newpage
	
	\thispagestyle{plain}
	\vspace*{\fill}
	{\Large Kurzfassung\par}
	\begin{justify}
		\vspace{1cm}
		Während unseres Studiums für Angewandte Informatik durchliefen wir die Vorlesungen Systemprogrammierung und Grafische Bedienoberflächen. In den dazugehörigen Praktika war unsere Aufgabe das Programmieren eines Spieles. Diese wirkten veraltet und überholt, genauso wie die Entwicklungswerkzeuge welche dafür genutzt wurden.
		
		Aus diesem Grund ist der Gedanke dieses Projektes das Erstellen eines Spieles unter Benutzung der aktuellsten Entwicklungswerkzeuge und Techniken. Dafür wird zum einen auf Serverseite \textit{CMake} für die Kompilierung und Clientseitig \textit{JavaFX} für die Entwicklung genutzt. Diese Werkzeuge sowie das programmierte Spiel werden in dieser Ausarbeitung beschrieben und erläutert.
	\end{justify}
	\newpage
		
	\thispagestyle{plain}
	\tableofcontents
	\newpage
	\pagenumbering{arabic}
	
	\section{Einführung}\label{sec:einfuerung}
	Das Studium der Angewandten Informatik umfasst ein weites Feld an Programmiersprachen und Techniken. Dabei kann aufgrund der beschränkten Zeit des Studiums das meiste nur in den Grundlagen oder leicht fortgeschrittenen Teilen behandelt werden. Dadurch fallen einige in der Industrie weitverbreitete Techniken und genutzte Werkzeuge unter den Tisch.
	
	Sehr deutlich ist das in den Vorlesungen Systemprogrammierung und Grafische Bedienoberflächen ersichtlich.
	
	\subsection{\acf{Sysprog}}\label{subsec:sysprog}
	In der Vorlesung \ac{Sysprog} wird großes Augenmerk darauf gelegt einen Server in der Hochsprache C zu programmieren und mithilfe eines vorbereiteten Client darauf zu verbinden. Wichtige Bestandteile der Server Programmierung ist die Nutzung von Sockets\footnote{vom Betriebssystem bereitgestellte Kommunikationsschnittstelle zwischen Programmen}, Pipes\footnote{Datenstrom zwischen zwei Prozessen durch einen Puffer}, Mutexe\footnote{englisch: \textbf{mut}ual \textbf{ex}clusion} und Semaphore\footnote{Datenstruktur zur Verwaltung beschränkter Ressourcen}.
	
	Für das kompilieren des Servers wird ein bereitgestelltes CMake-File genutzt, an dem die Studenten jedoch nichts ändern müssen. Doch genau dieses erstellen von CMake-Files ist ein wichtiger Bestandteil in der Industrie. Sie ist alltäglich in der Softwareentwicklung und sollte aus diesem Grund auch Bestandteil der \ac{Sysprog} Vorlesung sein.
	
	\subsection{\acf{GraBo}}\label{subsec:grabo}
	Die Vorlesung \ac{GraBo} bildet das Gegenstück zur \ac{Sysprog} Vorlesung. Hier wird der Server bereitgestellt und die Studenten bekommen die Aufgabe einen Client dafür zu schreiben. Hierbei wird momentan auf Java, insbesondere auf die Swing-Klassen, gesetzt.
	
	Java-Swing ist jedoch bereits in die Jahre gekommen und wird immer mehr von JavaFX abgelöst.
	
	\subsection{Schiffe versenken}\label{subsec:battleship}
	Das Spiel, auf dessen Grundregeln dieses Projekt beruht, ist allgemein als Schiffe versenken bekannt. Da es ein ziemlich altes Spiel ist und im digitalen Zeitalter analoge Spiele nicht mehr jedermann bekannt ist. Aus diesem Grund folgt nun eine kurze Spielbeschreibung.
	
	Ziel des Spiels ist es die gegnerische Flotte zu versenken, bevor die eigene dem anheim gefallen ist.
	
	Vor sich hat jeder Spieler nun sein Verteidigungs- und Angriffsfeld. Auf dem Verteidigungsfeld liegen seine Schiffe und auf dem Angriffsfeld platziert jeder Spieler seine Angriffe. 
	
	Zu Beginn wird dafür von jedem Spieler sein Verteidigungsfeld (quadratisch im Format $10\times10$), mit dem ihm zur Verfügung gestellten Schiffen, bestückt. Dabei ist das eigene Spielfeld für den Gegenspieler nicht ersichtlich. Eine beispielhafte Bestückung ist in folgender Abbildung zu sehen:
	
	\hspace{2cm}
	\begin{battleship}[rows=10,columns=10]
		\placeship{H}{1}{1}{4}
		\placeship{H}{4}{4}{3}
		\placeship{H}{2}{8}{2}
		\placeship{V}{10}{3}{5}
		\shipH{1,2,3,4,5,6,7,8,9,10}
		\shipV{10,9,8,7,6,5,4,3,2,1}	
	\end{battleship}
	\vspace{1cm}
	
	Nach dem platzieren der Schiffe darf nun ein Spieler beginnen. Dieser darf nun einen Angriff platzieren. Dafür wird dem Gegenspieler das Feld mitgeteilt welches angegriffen werden soll. Der Gegenspieler teilt daraufhin mit ob der Spieler einen Treffer gelandet hat oder ins Wasser geschossen hat.
	
	\hspace{3cm}
	\begin{battleship}[rows=10,columns=10,scale=0.75,title=Treffer]
		\placeship{H}{1}{1}{4}
		\placeship{H}{4}{4}{3}
		\placeship{H}{2}{8}{2}
		\placeship{V}{10}{3}{5}
		\shipH{1,2,3,4,5,6,7,8,9,10}
		\shipV{10,9,8,7,6,5,4,3,2,1}
		\placewater{2}{1}	
	\end{battleship}
	\vspace{1cm}
	
	\hspace{3cm}
	\begin{battleship}[rows=10,columns=10,scale=0.75,title=Miss]
		\placeship{H}{1}{1}{4}
		\placeship{H}{4}{4}{3}
		\placeship{H}{2}{8}{2}
		\placeship{V}{10}{3}{5}
		\shipH{1,2,3,4,5,6,7,8,9,10}
		\shipV{10,9,8,7,6,5,4,3,2,1}
		\placewater{6}{1}	
	\end{battleship}
	\vspace{1cm} 
	
	Bei einem Treffer darf der Spieler noch einmal angreifen, bei einem Fehlschuss ist der Gegenspieler an der Reihe. Wird ein Schiff durch einen Schuss versenkt muss dies dem Gegenspieler mitgeteilt werden.
	
	\hspace{2cm}
	\begin{battleship}[rows=10,columns=10]
		\placeship{H}{1}{1}{4}
		\placeship{H}{4}{4}{3}
		\placeship{H}{2}{8}{2}
		\placeship{V}{10}{3}{5}
		\shipH{1,2,3,4,5,6,7,8,9,10}
		\shipV{10,9,8,7,6,5,4,3,2,1}
		\placewater{1}{1}
		\placewater{2}{1}
		\placewater{3}{1}
		\placewater{4}{1}
		\placewater{7}{6}
		\placewater{1}{3}
		\placewater{3}{4}
		\placewater{2}{5}
		\placewater{7}{5}
		\placewater{8}{10}	
	\end{battleship}
	\vspace{1cm}
	 
	Dieses Prozedere wird solange fortgeführt bis ein Spieler keine intakten Schiffe mehr besitzt. Der Spieler der den letzten Treffer gelandet hat ist dadurch der Sieger.
	
	\hspace{2cm}
	\begin{battleship}[rows=10,columns=10]
		\placeship{H}{1}{1}{4}
		\placeship{H}{4}{4}{3}
		\placeship{H}{2}{8}{2}
		\placeship{V}{10}{3}{5}
		\shipH{1,2,3,4,5,6,7,8,9,10}
		\shipV{10,9,8,7,6,5,4,3,2,1}
		\placewater{1}{1}
		\placewater{2}{1}
		\placewater{3}{1}
		\placewater{4}{1}
		\placewater{7}{6}
		\placewater{1}{3}
		\placewater{3}{4}
		\placewater{2}{5}
		\placewater{7}{5}
		\placewater{8}{10}
		\placewater{4}{4}
		\placewater{5}{4}
		\placewater{6}{4}
		\placewater{2}{8}
		\placewater{3}{8}
		\placewater{10}{3}
		\placewater{10}{4}
		\placewater{10}{5}
		\placewater{10}{6}
		\placewater{10}{7}
		\placewater{7}{1}
		\placewater{9}{2}
		\placewater{3}{3}
		\placewater{8}{3}
		\placewater{1}{7}
		\placewater{4}{7}
		\placewater{6}{9}
		\placewater{10}{9}
		\placewater{1}{10}
		\placewater{4}{10}	
	\end{battleship}
	\vspace{1cm}
	
	Schiffe versenken ist im Original ein Spiel welches mit den minimalistischen Voraussetzungen, Stift und Papier, auskommt. Die genaue Herkunft ist bis heute unbekannt, konnte jedoch durch Zeitzeugen auf das späte 19. Jahrhundert zurückverfolgt werden \cite{warwitzspiele}.
	
	\newpage
	
	\section{Begrifflichkeiten}\label{sec:begriffe}
	Nachfolgend einige Begriffe welche innerhalb des Berichtes genutzt wurden und den Textfluss zerstreuen würden, würde man sie an den platzierten Stellen noch erläutern.
	
	\textbf{Programmiersprache:} ''System von Wörtern und Symbolen, die zur Formulierung von Programmen für die elektronische Datenverarbeitung verwendet werden'', dies ist die offizielle Beschreibung einer Programmiersprache \cite{duden}. Vereinfacht erläutert ist eine Programmiersprache wie eine eigene Sprache anzusehen: Sie verfügt sowohl über eine Grammatik, als auch über eine Wortschatz und Regeln. Jede Programmiersprache kann von einem Computer verstanden und interpretiert werden. Sie werden genutzt um Algorithmen und Abläufe in ein Programm zu packen.
	
	\textbf{Compiler:} Der Compiler ist, simpel ausgedrückt, ein Dolmetscher zwischen dem Quellcode/der Programmiersprache und dem Computer. Er wandelt die Abläufe und Algorithmen, welche im Programm hinterlegt wurden in eine für den Computer verständliche ''Sprache'' um.
	
	\textbf{Compiler-Befehle:} Dies sind spezielle Befehle die dem Compiler mitgegeben werden um zum Beispiel die Zeilen eines Quelltextes anders zu interpretieren als er es standardmäßig tun würde. Auch müssen so die Pfade zu den Dateien, der Ablageort des Makefiles, der Name der Executable-Datei und vieles mehr übergeben werde.
	
	\textbf{Mutex:} Ein Mutex ist ein Verfahren um bei Prozessen die zur gleichen Zeit arbeiten (beispielsweise Threads) zu verhindern das diese auf ein und die selbe Datei oder Variable zugreifen und so inkonsistenten verursachen. Der Ablauf dieses Kritischen Abschnittes ist in folgenden Pseudocode beispielhaft dargestellt.
	
	\begin{lstlisting}[style=CStyle,lastline=5,numbers=none,caption=Pseudocode Mutex]
		do_something(x)
		mutex_lock(s)					//sperren des Mutex mithilfe Variable 's'
		f(x)									//konsistenter Zugriff ist gegeben
		mutex_unlock(s)				//freigeben des Mutex
		do_something_else(x)
	\end{lstlisting}
	
	\textbf{Semaphore:} Semaphore sind Datenstrukturen die aus einem Zahloperator und Anweisungen bestehen. Wie Mutexe eignen auch Sie sich für die Koordination asynchroner Abläufe. Semaphore wurden von dem Informatikpionier E. W. Dijkstra im Jahre 1968 vorgestellt \cite{semaphore}. Ein beispielhaften Pseudocode ist in Code \ref{lst:sem}.
	
	In der ersten Funktion ist die Initialisierung des Semaphore zu sehen, die Funktion \texttt{P(s)} dekrementiert den Semaphore \texttt{s} und blockiert den Prozess, sollte der Wert des Semaphore unter null fallen. Die letzte Funktion \texttt{V(s)} schließlich dient dazu den Wert des Semaphore zu inkrementieren und andere Prozesse, die aktuell blockiert sind, freizugeben.
	
	\begin{lstlisting}[style=CStyle,lastline=18,numbers=none,caption=Pseudocode Semaphore,label=lst:sem]
		init(i) {
			s->value = i
			return
		}
		
		P(Sem s) {
			s->value--;
			if(s->value < 0)
				//blockieren des Prozesses
			return
		}
		
		V(s) {
			s->value++;
			if(s->value <= 0)
				//freigeben eines Prozesses, der blockiert ist
			return
		}
	\end{lstlisting}
	
	\textbf{Socket:} Sockets sind Werkzeuge welche von Betriebssystemen bereitgestellt werden. Sie dienen der Kommunikation zwischen zwei Programmen auszutauschen. Ein deutliches Beispiel ist zwischen dem Webbrowser, auf dem das Online-Banking läuft und dem Programm zum autorisieren von Überweisungen die über das Online-Banking abgewickelt wird. Abbildung \ref{fig:socket} zeigt den Ablauf der Socketverbindung sinnbildlich.
	
	\begin{figure}[!h]
		\centering
		\includegraphics[width=0.6\linewidth]{sockets.png}
		\begin{flushright}
			{\tiny Quelle: Informatik-Treff}
		\end{flushright}
		\caption{Bildliche Darstellung einer Socketverbindung}
		\label{fig:socket}
	\end{figure}
	
	\textbf{Pipes:} Pipes kann man sich, wie der Name schon sagt, als eine Leitung vorstellen. In diese werden Daten eingegeben und ausgelesen, nach dem FIFO-Prinzip\footnote{\textbf{F}irst-\textbf{I}n-\textbf{F}irst-\textbf{O}ut}. Dies wird in Programmen genutzt um zwischen Prozessen oder Threads Daten auszutauschen.
	
	Bild einfügen!!!
	
	\textbf{RFC:} Als RFC\footnote{\textbf{R}equest \textbf{F}or \textbf{C}omment} bezeichnet man gemeinhin ein technisches oder organisatorisches Dokument, welches sich auf Kommunikationsprotokolle im Internet bezieht. Heutzutage gibt es viele Arten von RFCs, auch für gänzlich andere Gebiete. Beispielhaft ist unten ein Auszug aus der RFC für UDP-Pakete\footnote{\textbf{U}ser \textbf{D}atagram \textbf{P}rotocol} zu sehen.
	
	\VerbatimInput[label=\fbox{\color{Black}UDP-RFC}]{rfc.txt}
	
	\newpage
	
	\section{Anforderungsanalyse}\label{sec:conditions}
	Um den Anforderungen des Informatikprojektes gerecht zu werden wurde die Programmierung sowie Dokumentation des Projektes zwischen uns aufgeteilt.
	
	Serverseitig wurde alles von Andreas umgesetzt, genauso beschäftigte er sich im Bericht mit der Einführung, der Anforderungsanalyse, dem Server, CMake und der Zusammenfassung.
	
	Florian führte sämtliche Programmierungen am Client in JavaFX durch und bearbeitete die Teilbereiche Begrifflichkeiten, Client, JavaFX und die Ergebnisse.
	
	\subsection{Grundgedanke}
	Da der Grundgedanke dieses Projektes, die Ersetzung des aktuellen Programms in \ac{Sysprog} sowie \ac{GraBo} war, mussten auch die Programmiersprachen in den Grundlagen dem bereits genutzten entsprechen. Aus diesem Grund stand es außer Frage den Server in C und den Client in Java zu programmieren.
	
	\subsection{Server-Programmierung}
	Die Programmierung des Servers kann auf viele Arten und Weisen geschehen und muss dementsprechend vorher genauestens überdacht werden. Auch gab es Vorgaben welche erfüllt werden mussten. Zum einen war die Nutzung von Pipes und Sockets für den Datenaustausch, sowie die Einbindung von asynchroner Verarbeitung, gefordert. Sockets und Pipes sollten genutzt werden um die Kommunikation zwischen den Teilnehmern zu gewährleisten. Die Nutzung einer asynchronen Technik muss immer dann erfolgen wenn Dateien oder Variablen von zwei unterschiedlichen Teilnehmern genutzt werden soll.
	
	\subsection{Compiler}
	Der genutzte Compiler für den Server sollte dem aktuellen entsprechen. Dies liegt zum einen an der Möglichkeit den gesamten Server in der aktuellen Laborumgebung nutzbar zu machen, zum anderen erleichtert das den Studenten den Zugang zum Projekt. So können sie mit ihren aktuellen Kenntnissen welche im ersten und zweiten Semester vermittelt werden weitgehend selbstständig arbeiten.
	
	\subsection{Compiler-Befehle}
	Die Nutzung von Compiler-Befehlen im klassischen Sinne soll komplett entfernt werden. Dies ist nicht mehr sinngemäß und erschwert den Studenten nur unnötig die Arbeit. Aus diesem Grund soll ein Programmierwerkzeug genutzt werden welches die Compiler-Befehle selbstständig ausführt, jedoch nicht ohne die richtige Verwaltung und Nutzung der Studenten zu fordern. So wird der aktuelle Stand der Technik vermittelt und auch die Erlernung neuer, praxisnaher Methoden wird gefördert.
	
	\subsection{RFC}
	Für den Austausch von Nachrichten zwischen den Teilnehmern soll eine RFC genutzt werden. Dies soll den Studenten die Möglichkeit geben sämtliche Nachrichten welche nachzuvollziehen. Da RFCs bereits in den ersten beiden Semestern des Studiums eingeführt und behandelt werden sollte das Lesen und nachvollziehen selbiger keinerlei Probleme darstellen. 
	
	\subsection{Client-Programmierung}
	\newpage
	
	\section{Umsetzung}\label{sec:umsetzung}
	
	\subsection{Client}\label{subsec:client}
	
	\subsection{JavaFX}\label{subsec:javafx}
		
	\subsection{Server}\label{subsec:server}
	Bei der Umsetzung des Servers wurden viele Punkte beachtet und aufgegriffen welche in den folgenden Unterkapiteln genauer erläutert werden.
	
	\subsubsection{Struktur}\label{subsubsec:struct}
	Die allgemeine Struktur die sich für den Aufbau des Servers überlegt wurde weicht in den Grundzügen nicht von der bereits bekannten Struktur aus \ac{Sysprog} ab. Dadurch ist Studenten, die das Fach wiederholen müssen, die Möglichkeit gegeben sich leichter wieder in das Projekt einzufinden.
	
	Der Grundaufbau des Servers ist in Abbildung ... visuell dargestellt.
	
	\subsubsection{Threads}\label{subsubsec:threads}
	Das Programm kommt in seiner gesamten Laufzeit mit drei Threads aus. Darunter ist ein LogIn-Thread welcher die Client Anmeldung verwaltet. Dieser erzeugt nach erfolgreicher Anmeldung einen Client-Thread welcher dem Spieler zugeordnet wird. Da Schiffe versenken ein Zwei-Spieler Spiel ist werden auch nur zwei Anmeldungen und somit zwei Client-Threads zugelassen und erzeugt.
	
	Die Threads und deren Abhängigkeiten sind in Abbildung ... dargestellt. Dies erleichtert die Verdeutlichung und ermöglicht es Außenstehenden den Ablauf der Thread Erzeugung zu verstehen.
	
	\subsubsection{Map- \& Ship-Handling}\label{subsubsec:maps}
	Jedem Client-Thread wird direkt bei Erzeugung eine Karte zur Seite gestellt. Dies geschieht über den folgenden Initial Aufruf bei der Erzeugung der Map-Klasse.
	
	\begin{lstlisting}[style=CStyle,lastline=23,caption=Initialisierung der Karte]
	Map *init_map_matrix(int width, int height) {
		Map *m = malloc(sizeof(Map));
		int i,j;
		
		m->width = width;
		m->height = height;
		
		m->map = malloc(m->width * sizeof(int *));
		
		for(i=0; i<m->width; i++)
		{
			m->map[i] = malloc(m->height * sizeof(int));
			for(j=0; j<m->height; j++)
			m->map[i][j] = 0;
		}
		
		m->ships[0] = 1;
		m->ships[1] = 1;
		m->ships[2] = 1;
		m->ships[3] = 1;
		
		return m;
	}
	\end{lstlisting}
	
	Der Aufruf ist bewusst variabel gehalten was die Kartengröße angeht, um in möglichen nachfolgenden Projekten, die Größe änderbar zu machen. Anschließend wird nur noch der Speicher für die Karte reserviert und die Karte selbst leer erzeugt. Auch wird der Karte direkt übergeben wie viele Schiffe welchen Typs in dieser Partie eingesetzt werden. Dies kann in nachfolgenden Projekten ebenfalls variabel gestaltet werden.
	
	Diese Karte dient als Spielfeld und bekommt vom Client, vor Spielbeginn, die Position der Schiffe übermittelt. Dies geschieht über folgende Nachricht:
	
	\VerbatimInput[label=\fbox{\color{Black}SHP\_Message}]{SHP_Message.txt}
	
	Der Spieler muss also alle Schiffe auf dem Spielfeld platzieren und dies anschließend bestätigen. Durch bestätigen der Platzierung wird diese Nachricht mit den vier Schiffen erzeugt. Diese enthält dabei sowohl die nördlichste, beziehungsweise westlichste Position, des Schiffes (je nach Ausrichtung) sowie dessen Orientierung (vertikal oder horizontal). Die Länge des Schiffes wird über den Typ festgelegt (Schlachtschiff = 1, U-Boot = 2, ...).
	
	Dabei ist es wichtig die Länge der Nachricht zu überprüfen, da beide Spieler immer gleich viele Schiffe haben sollen. Auch müssen die Schiffe selbst überprüft werden, um festzustellen ob die hinterlegte Anzahl der Schiffe des selben Typs bereits erreicht wurde, um unfaire Partien zu vermeiden.
	
	Dies geschieht am Ende der Insert-Funktion:
	
	\begin{lstlisting}[style=CStyle,linerange={1-3,48-51},escapeinside={(*}{*)},caption=Funktion zum Einfügen eines neuen Schiffes. Im Fokus steht die Verringerung der gültigen Schiffe]
		int insert_ship(Map* m, int ship, int x, int y, int orientation)
		{
			(* \vdots *)
			int i, size;
			
			switch(ship)
			{
				case AIRCRAFT_CARRIER:
					size = AIRCRAFT_CARRIER_SIZE;
					break;
				case BATTLESHIP:
					size = BATTLESHIP_SIZE;
					break;
				case SUBMARINE:
					size = SUBMARINE_SIZE;
					break;
				case PATROL_BOAT:
					size = PATROL_BOAT_SIZE;
					break;
			}
			
			if (orientation == VERTICAL){
				if (x >= m->height || y+size-1 >= m->width){
					return -1;
				} else {
					for (i=y; i<y+size; i++)
						if (m->map[i][x] != 0)
							return -1;
			
					for (i=y; i<y+size; i++)
						m->map[i][x] = SHIP;
				}
			}
			
			if (orientation == HORIZONTAL){
				if (y >= m->width || x+size-1 >= m->height){
					return -1;
				} else {
					for (i=x; i<x+size; i++)
						if (m->map[y][i] != 0)
							return -1;
			
					for (i=x; i<x+size; i++)
						m->map[y][i] = SHIP;
				}
			}
			
			m->ships[ship]--;
			
			return 0;
		}
	\end{lstlisting}
	
	Dabei wird der Karte mitgeteilt um welchen Typ Schiff es sich handelt und dieser um eins verringert.
	
	\subsubsection{Attack-Handling}
	Da nun die Karte aufgebaut und die Schiffe platziert wurden kann der erste Spieler mit seinem Angriff beginnen. Die Angriffsverwaltung läuft über zwei Nachrichten ab - den Attack-Request (ARQ) und den Attack-Response (ARE). Diese sind nachfolgend zu sehen.
	
	\VerbatimInput[label=\fbox{\color{Black}Attack\_Messages}]{Attack_Messages.txt}
	
	Nachdem der erste Spieler seinen Angriff in Auftrag gegeben hat wird vom Server die ARQ-Nachricht empfangen. Diese Nachricht beinhaltet sowohl das angegriffene Feld, als auch ein Flag\footnote{englisch: Flagge; in der Informatik: Kennzeichnung eines bestimmten Status} das kennzeichnet woher die Nachricht kommt. Sollte das Flag indizieren dass die Nachricht vom Socket - also vom Client - kommt, wird Sie über die Pipe an den zweiten Client-Thread weitergeleitet. Da ein Client-Thread immer nur seine eigene Karte kennt kann er keine Aussage über einen eingehenden Angriff treffen ohne den anderen Thread zu befragen.
	
	Kommt die Nachricht hingegen von der Pipe, weiß der Client-Thread das er angegriffen wird und führt folgende Funktion aus:
	
	\begin{lstlisting}[style=CStyle,lastline=14,caption=Funktion zur Bearbeitung eines Angriffs]
		int attack_ship(Map *m, int x, int y)
		{
			if (x > m->width || x < 0 || y > m->width || y < 0)
				return -1;
			
			if (m->map[y][x] == SHIP || m->map[y][x] == HIT)
			{
				m->map[y][x] = HIT;
				return 1;
			} else {
				m->map[y][x] = MISS;
				return 0;
			}
		}
	\end{lstlisting}
	
	Hierbei wird die Karte angefragt, ob sich an der übergebenen Position ein Schiff befindet. Ist dies der Fall wird eine ARE-Nachricht erzeugt, welche als Ergebnis (result) den Wert eins übergibt. Diese Nachricht wird dann sowohl über den Socket, als auch die Pipe verschickt. Dies wird gemacht, damit beide Spieler Feedback über den Angriff erhalten.
	
	Dieser Vorgang findet für jeden eingehenden Angriff statt. Zusätzlich zu der Verarbeitung der Angriffe wird nach dem Absenden der ARE-Nachricht eine Abfrage der Karte gestartet.
	
	\begin{lstlisting}[style=CStyle,lastline=11,caption=Funktion zur Überprüfung der Karte auf noch vorhandene Schiffe]
		int check_map(Map* m)
		{
			int i, j;
			
			for(i=0; i<m->width; i++)
				for(j=0; j<m->height; j++)
					if (m->map[i][j] == SHIP)
						return 1;
			
			return 0;
		}
	\end{lstlisting}
	
	Dabei wird geprüft ob sich auf der Karte noch ein Feld befindet, auf dem ein Schiff steht. Ist dies nicht der Fall wird die GameOver-Nachricht (GOV) an alle Teilnehmer geschickt.
	
	\VerbatimInput[label=\fbox{\color{Black}GOV\_Message}]{GOV_Message.txt}
	
	Diese übergibt sowohl die Platzierung des Spielers (Sieger oder Verlierer) und die Punktzahl. Letztere ist in diesem Projekt jedoch nicht in Benutzung und nur der Vollständigkeit halber vorhanden, für mögliche Erweiterungen in zukünftigen Projekten.
	
	\subsection{CMake}\label{subsec:cmake}
	Für die Erzeugung des Makefiles\footnote{Dateien die zur Erzeugung einer Executable genutzt werden} des Servers musst ein Entwicklungswerkzeug gewählt werden welches auch in der Laboren der Hochschule den Studenten zur Verfügung steht. Nach Recherchen und Absprache mit den Professoren wurde sich für CMake entschieden \cite{cmake}. 
	
	CMake ist ein Entwicklungswerkzeug welches besonders für größere Projekte geeignet ist und auf allen UNIX-Systemen standardmäßig enthalten und kann somit ohne Einschränkung von jedem genutzt werden. Es erfordert außer dem Werkzeug selbst noch eine extra Datei. Diese muss zwingend \textit{CMakeLists.txt} heißen, da sie sonst nicht von CMake gefunden wird. Nach dem anlegen dieser Datei muss nur noch ein Ordner angelegt werden, in dem die ganzen erzeugten Dateien abgelegt werden. Eine gute Struktur kann so aussehen:
	
	\dirtree{%
		.1 / .
		.2 build .
		.2 src .
		.3 C-Files .
		.3 H-Files .
		.2 CMakeLists .
	}
	
	Es gibt eine übergeordnete \textit{CMakeLists-Datei}, welche die Header und Programm Dateien im \textit{src-Ordner} verlinkt bekommt und anschließend aus diesen ein Makefile erzeugt, welches im \textit{build-Ordner} abgelegt wird.
	
	Der Aufbau der \textit{CMakeLists-Datei} wird beispielhaft an der Datei des Projektes erklärt.
	
	\VerbatimInput[label=\fbox{\color{Black}Im Ordner der Resourcen}]{CMakeLists_bin.txt}
	
	Im Projekt wurden zwei Dateien verwendet. Dies liegt daran dass es möglichst generisch gehalten werden sollte, um spätere Änderungen und Erweiterungen ohne viel Aufwand möglich zu machen. In der oberen Datei ist das Verlinken der \textit{C-Dateien} zu sehen, sowie das hinzufügen der Thread-Bibliothek. Dieser Schritt ist notwendig, da die Dateien sonst den include nicht finden und die Executable nicht gebaut werden kann.
	
	In der übergeordneten Datei wird lediglich der Unterordner bekannt gemacht, sowie ein zusätzliches Compiler-Flag gesetzt. Ohne dieses Flag kommt es zu Fehlern beim bauen.
	
	\VerbatimInput[label=\fbox{\color{Black}Übergeordnet}]{CMakeLists.txt}
	
	Da alles für das Projekt bereitgestellt wurde, muss nun nur noch der Befehl\linebreak \texttt{cmake ..} innerhalb des \textit{build-Ordners} ausgeführt werden, welches die Makefiles erzeugt. Nachdem dies geschehen ist muss das Programm gebaut werden, indem \texttt{make} ausgeführt wird und die Executable wird erzeugt.
	
	CMake erleichtert durch seine generische Arbeitsweise und das leichte editieren der Dateien die Arbeit mit großen Daten- und Verzeichnismengen deutlich.
	
	\newpage
	
	\section{Ergebnisse}\label{sec:result}
	
	\newpage
	
	\section{Zusammenfassung}\label{sec:zusammenfassung}
	
	\newpage
	\addcontentsline{toc}{section}{Literaturverzeichnis}
	\printbibliography
\end{document}