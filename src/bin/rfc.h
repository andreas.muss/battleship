#ifndef RFC_H
#define RFC_H
#define TRUE 1
#define FALSE 0
#define MAX_PLAYERS 2	//Anzahl der maximal möglichen Spieler
#define RFC_VERSION 1	//aktuelle RFC-Version

#include <pthread.h>
#include <semaphore.h>
#include <stdint.h>

enum{
	LRQ_TYPE = 1,      // Login Request
	LOK_TYPE,          // Login OK
    SHP_TYPE,          // Ships
	STG_TYPE,          // Start Game
	ARQ_TYPE,          // Attack Request
	ARE_TYPE,          // Attack Response
	GOV_TYPE,          // Game Over
	ERR_TYPE = 255,    // ERROR

};


typedef struct __attribute__((packed)){
	int userID;
	pthread_t user_thread;
	pthread_t score_thread;
	uint32_t score;
	int socket;
	int rank;
	int shipAmount;
}pi;


typedef struct __attribute__((packed)){
	uint32_t score;
	uint8_t id;
}pl;


typedef struct __attribute__((packed)){
	uint8_t type;
	uint16_t length;
	uint8_t rfcversion;
	uint8_t maxplayers;
	uint8_t clientid;
}LOK;


typedef struct __attribute__((packed)){
    uint8_t type;
    uint16_t length;
    uint8_t xPos;
    uint8_t yPos;
    uint8_t result;
    uint8_t clientid;
}ARE;


typedef struct __attribute__((packed)){
	uint8_t type;
	uint16_t length;
	uint8_t subtype;
	uint8_t message[200];
}ERR;


typedef struct{
    pthread_mutex_t playerlist_mutex;
	pthread_mutex_t socket_mutex;
	pthread_mutex_t sem_mutex;
	pthread_mutex_t server_data_mutex;
    pthread_mutex_t map_mutex;
}ms;


typedef struct __attribute__((packed)){
	uint8_t type;
	uint16_t length;
	uint8_t rank;
	uint32_t score;
}GOV;


LOK createLOK(int id);
ARE createARE(int XPOS, int YPOS, int result);
GOV createGOV(int rank, int score);

ERR createERR(char message[], int subtype);

void sendLOK(int activePlayers, int client_socket);
void sendERR(int subtype, char message[], int client_socket);
void sendARE(int xPos, int yPos, int result, int client_socket);
void sendGOV();

#endif
