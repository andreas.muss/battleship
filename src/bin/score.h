#ifndef SCORE_H
#define SCORE_H
#include "rfc.h"

#include <semaphore.h>
#include <pthread.h>

void *score_handler(void* socket_desc);
void sortPlayers();
void createNewList();
void sendNewLST(int spielerAnzahl);
void setRanks();
void sortPlayerRanks(pi playerinfo[]);

#endif


