#ifndef CLIENTTHREAD_H
#define CLIENTTHREAD_H

void *client_thread(void* socket_desc);
void encodeMessage(char message[], int client_socket, int length);

void sigquit();

int getPlayerOfThread(int client_socket);

int GetGameIsRunning();
int GetActivePlayers();
int GetGameCancelled();

void CloseServer();

#endif
