#include "user.h"
#include "rfc.h"
#include "score.h"
#include "login.h"
#include "clientthread.h"
#include <string.h>

extern pi playerinfo[2];

//Pruefung auf den Status des GameLeaders
int isGameLeader(int client_socket){
	for(int i=0;i<2;i++){
		if(playerinfo[i].socket == client_socket){		//allsockets in playerinfo ändern
			if(playerinfo[i].userID == 0){		//playerlist in playerinfo ändern
				return 1;
			}else{
				return 0;
			}
		}
	}
    return 0;
}

//Sortierung der PlayerList
void sortPlayerList(int client_socket){

	//int activePlayers = getPlayerAmount(server_data);		//server_data in playerinfo ändern
	int activePlayers = GetActivePlayers();


	for(int i = 0; i < 2; i++)
	{
		if(playerinfo[i].socket == client_socket)		//allsockets in playerinfo ändern
		{
			while(i < 2)
			{
				// Socket in Socketliste aktualisieren
				playerinfo[i].socket=playerinfo[i+1].socket;		//allsockets in playerinfo ändern
				playerinfo[i+1].socket=0;		//allsockets in playerinfo ändern

				// UID aktualisieren
				playerinfo[i].userID=playerinfo[i+1].userID;
				playerinfo[i + 1].userID = 99;

				i++;
			}


		}
	}
}
