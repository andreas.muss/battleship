#include "util.h"
#include "clientthread.h"
#include "rfc.h"
#include "login.h"
#include "score.h"

#include <getopt.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <pthread.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>

extern mc myconfig;

int main(int argc, char **argv)
{
	setProgName(argv[0]);
	setDefaults();

	infoPrint("Battleship");
    
	int opt;
	while ((opt = getopt(argc, argv, "p:dmrsh")) != -1) {
		switch(opt){
				case 'p': 	myconfig.port=atoi(optarg);
                        break;
				case 'd': 	myconfig.debug=TRUE;
					  	debugEnable();
					  	break;
				case 'm': 	myconfig.monochrome=TRUE;
					  	styleEnable();
					  	break;
				case 'r': 	myconfig.receive=TRUE;
					  	break;
				case 's': 	myconfig.send=TRUE;
					  	break;
                case 'h':   printStartParameter();
                        return -1;
				default:
					  	return -1;
	    		}
	}

	infoPrint("Threads werden gestartet");
	debugPrint("Lock-File erfolgreich entfernt.");

	pthread_t login_thread, score_thread;
	int loginThread_OK, scoreThread_OK;

	// Login und Score Thread erstellen
	loginThread_OK = pthread_create(&login_thread, NULL, login_handler, NULL);
	scoreThread_OK = pthread_create(&score_thread, NULL, score_handler, NULL);

	infoPrint("Threads wurden gestartet");

	if(loginThread_OK<0){
		errorPrint("Fehler beim Login-Thread.");
		return -1;
	}

	if(scoreThread_OK<0){
		errorPrint("Fehler beim Score-Thread.");
		return -1;
	}

	createLockfile();

	pthread_join(login_thread, NULL);
	pthread_join(score_thread, NULL);


	// Auf Threads warten
	signal(SIGQUIT, sigquit);



	return 0;
}
