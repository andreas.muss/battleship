#include "score.h"
#include "login.h"
#include "clientthread.h"
#include "rfc.h"
#include "user.h"
#include "util.h"

#include <getopt.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <pthread.h>
#include <semaphore.h>

sem_t score_semaphore;

extern ms mutex_struct;

//Starten des Score-Threads
void *score_handler(void* socket_desc){
	infoPrint("Score-Thread wurde erfolgreich gestartet.");
	int activePlayers;
	while(1){
		sem_wait(&score_semaphore);
		//activePlayers=getPlayerAmount(server_data)-1;	//server_data in playerinfo ändern

		activePlayers = GetActivePlayers();
	}
}
