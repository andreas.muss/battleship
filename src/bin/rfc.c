#include "rfc.h"
#include "user.h"
#include "clientthread.h"
#include "score.h"
#include "login.h"
#include "util.h"
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

extern mc myconfig;

ms mutex_struct;
pi playerinfo[2];
pl playerlist[2];

//LoginResponseOK Nachricht
LOK createLOK(int id)
{
	LOK lok;
	lok.type=LOK_TYPE;
	lok.length=htons(3);
	lok.rfcversion= RFC_VERSION;
	lok.maxplayers= MAX_PLAYERS;
	lok.clientid=id;
	return lok;
}

//AttackResponse Nachricht
ARE createARE(int XPOS, int YPOS, int result)
{
    ARE are;
    are.type=ARE_TYPE;
    are.length=htons(3);
    are.xPos=XPOS;
    are.yPos=YPOS;
    are.result=result;
    
    return are;
}

//ErrorWarning Nachricht
ERR createERR(char message[], int subtype)
{
	ERR err;
	err.type=ERR_TYPE;
	err.subtype=subtype;
	err.length=htons(strlen(message)+1);
	strcpy(err.message, message);
	err.message[strlen(message)]='\0';

	return err;
}

typedef struct {
	int playerinfo_index;
	uint32_t score;
} rankHelper;

//GameOver Nachricht
GOV createGOV(int rank, int score)
{
	GOV gov;
	gov.type=GOV_TYPE;
	gov.length=htons(5);
	gov.rank=rank+1;
	gov.score=htonl(score);
	return gov;
}

//Error Nachricht senden
void sendERR(int subtype, char message[], int client_socket)
{
	ERR err;
	err = createERR(message, subtype);
	if(myconfig.send){
		debugHexdump(&err, strlen(message)+4, "S==>C");
	}
	send(client_socket, &err, strlen(message)+4,0);
}

//LoginResponseOK Nachricht senden
void sendLOK(int activePlayers, int client_socket)
{
	LOK lok;
	lok = createLOK(activePlayers);
	if(myconfig.send){
		debugHexdump(&lok, sizeof(lok), "S==>C");
	}
	send(client_socket, &lok, sizeof(lok),0);


	//Kritischer Abschnitt -- START --
	pthread_mutex_lock (&mutex_struct.playerlist_mutex);
	playerlist[activePlayers].score=0;		//playerlist in playerinfo ändern
	playerlist[activePlayers].id=activePlayers;		//playerlist in playerinfo ändern
	pthread_mutex_unlock (&mutex_struct.playerlist_mutex);
	//Kritischer Abschnitt -- ENDE --

}

//AttackResponse Nachricht senden
void sendARE(int xPos, int yPos, int result, int client_socket)
{
    ARE are;
    are = createARE(xPos, yPos, result);
    if(myconfig.send)   {
        debugHexdump(&are, sizeof(are), "S==>C");
    }
    send(client_socket, &are, sizeof(are),0);
}


//GameOver Nachricht senden
void sendGOV()
{
	rankHelper reihenfolge[2];
	for(int v = 0; v < 2; v++) {

		reihenfolge[v].score = playerinfo[v].score;
		reihenfolge[v].playerinfo_index = v;
	}

	for(int x = 0; x < 2 - 1; x++) {
		for (int j = 0; j < 2 - x - 1; ++j)   {
			if (reihenfolge[j].score < reihenfolge[j + 1].score)   {
				rankHelper tmp = reihenfolge[j];

				reihenfolge[j].score = reihenfolge[j + 1].score;
				reihenfolge[j].playerinfo_index = reihenfolge[j + 1].playerinfo_index;

				reihenfolge[j+1].score = tmp.score;
				reihenfolge[j+1].playerinfo_index = tmp.playerinfo_index;

				reihenfolge[j + 1] = tmp;
			}
		}
	}
	for(int i=0; i < MAX_PLAYERS; i++){
		GOV gov;
		gov = createGOV(i , playerinfo[reihenfolge[i].playerinfo_index].score);
		if(myconfig.send){
			debugHexdump(&gov, sizeof(gov), "S==>C");
		}
		send(playerinfo[reihenfolge[i].playerinfo_index].socket, &gov, sizeof(gov),0);		//allsockets in playerinfo ändern
	}
}
